This repository contains configuration files for a Samba server intended for use by a PlayStation 2 running [Open PS2 Loader](https://github.com/ifcaro/Open-PS2-Loader) (OPL).

The Samba configuration is contained in the file `smb.conf` (`/etc/samba/smb.conf`). The server's static network configuration is defined in `rc.local` (`/etc/rc.local`).

Installing these two configuration files onto a server running Samba will cause it to adjust its network interface to use a predictable address, and host an unsecured (guest-writable) Samba share `PS2SMB` from `/ps2`, intended for use by OPL.

The general idea here is that the server (ideally a physically small device that consumes little power, like a Raspberry Pi) should be connected directly to the PlayStation 2's network adapter using an Ethernet (optionally crossover) cable.

A network configuration for OPL that matches the server's static network configuration is contained in `conf_network.cfg`, which OPL will look for at `/OPL/conf_network.cfg` on either memory card.

Once the server is connected, properly configured, running, and hosting something visible (i.e. a PlayStation 2 game), a copy of OPL with the correct network configuration will be able to retrieve disc images from the server and boot them.

This repository does not intend to document or provide very much information at all about OPL itself; if you're using this, it's expected that you already know how to use OPL. If this isn't the case, please refer to the [OPL documentation project](https://bitbucket.org/ShaolinAssassin/open-ps2-loader-0.9.3-documentation-project/wiki/Home).
